# React Proyecto 2 - Presupuesto
Sistema de presupuesto que almacena las cantidades y genera un restante.

[Link to Web:](https://serene-jepsen-9d847b.netlify.app/)

## ¿Herramientas utilizadas?
- Short_uuid
- useEffect
- useState
- Formulario básico


## ¿Cómo funciona?
- Se recoge la información del presupuesto mediante el formulario, se almacena y luego gracias a UseState se puede hacer que dicha sección desaparezca para dar paso a una sección de ingresar gastos, una vez que se ingresan los datos, son enviados al Listado y se actualiza el monto restante.


## ¿Dudas?

¿Por qué se utiliza "const" en casos como "const [ cantidad, guardarCantidad ] = useState(0);" Si la variable cantidad puede ser cambiada mediante el método?
